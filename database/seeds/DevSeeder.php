<?php

use App\Models\Image;
use App\Models\Movement;
use App\Models\User;
use App\Traits\SeedHelper;
use Illuminate\Database\Seeder;

class DevSeeder extends Seeder
{
    use SeedHelper;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->prepareSeed();
        $this->call(DatabaseSeeder::class);
        $this->call(UsersDevSeeder::class);
        $this->finishSeed();

        $this->call(AuthorizationSeeder::class);

        User::all()
            ->each(function ($user) {
                factory(Movement::class, 500)->create([
                    'user_id' => $user->id,
                ]);
            });

        Movement::where('user_id', 2)
            ->inRandomOrder()
            ->take(1)
            ->get()
            ->each(function ($movement) {
                factory(Image::class)->create([
                    'movement_id' => $movement->id,
                ]);
            });
    }
}
