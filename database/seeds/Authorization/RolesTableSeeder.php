<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    protected $roles = [
        [
            'name' => 'ADMIN',
            'title' => 'Administrador',
        ],
        [
            'name' => 'MERCHANT',
            'title' => 'Comerciante',
        ],
        [
            'name' => 'CLIENT',
            'title' => 'Cliente',
        ],
        [
            'name' => 'TRANSPORTER',
            'title' => 'Transportador',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->roles)->each(function ($role) {
            \App\Models\Role::create($role);
        });
    }
}
