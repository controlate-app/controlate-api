<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    protected $permissions = [
        'users' => [
            [
                'name' => 'users.show',
                'title' => 'Consultar Usuarios',
                'roles' => ['ADMIN'],
            ],
            [
                'name' => 'users.create',
                'title' => 'Crear Usuarios',
                'roles' => ['ADMIN'],
            ],
            [
                'name' => 'users.edit',
                'title' => 'Editar Usuarios',
                'roles' => ['ADMIN'],
            ],
            [
                'name' => 'users.delete',
                'title' => 'Eliminar Usuarios',
                'roles' => ['ADMIN'],
            ],

        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->permissions)->each(function ($abilities, $group) {
            foreach ($abilities as $ability) {
                $permission = \App\Models\Permission::forceCreate(
                    array_except($ability + ['group' => $group], ['roles'])
                );

                foreach ($ability['roles'] as $rol) {
                    \App\Models\Role::findByName($rol)->givePermissionTo($permission);
                }
            };
        });
    }
}
