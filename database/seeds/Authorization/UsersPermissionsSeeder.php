<?php

use Illuminate\Database\Seeder;

class UsersPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Models\Role::findByName('ADMIN');
        \App\Models\User::all()->each->assignRole($role);
    }
}
