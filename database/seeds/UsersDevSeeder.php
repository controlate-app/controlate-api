<?php

use App\Models\User;
use App\Models\UserCc;
use Illuminate\Database\Seeder;

class UsersDevSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaultUsers() as $user) {
            User::forceCreate(
                array_merge($user, ['password' => bcrypt('qwerty1234'), 'email_verified_at' => now()])
            );
        }

        factory(User::class, 3)->create();
    }

    public function defaultUsers()
    {
        return collect([
            [
                'first_name' => 'admin',
                'last_name' => 'admin',
                'email' => 'admin@gmail.com',
                'phone' => '123456',
            ],
            [
                'first_name' => 'Nacho',
                'last_name' => 'Fassini',
                'email' => 'nachofassini@gmail.com',
                'phone' => '123456',
            ],
        ]);
    }
}
