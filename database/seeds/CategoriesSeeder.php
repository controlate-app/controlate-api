<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaultCategories() as $item) {
            Category::create($item);
        }
    }

    public function defaultCategories()
    {
        return collect([
            [
                'icon' => 'home',
                'name' => 'Home',
            ],
            [
                'icon' => 'car',
                'name' => 'Car',
            ],
            [
                'icon' => 'ios-pizza',
                'name' => 'Food',
            ],
            [
                'icon' => 'beer',
                'name' => 'Drinks',
            ],
            [
                'icon' => 'ios-help',
                'name' => 'Other',
            ],
        ]);
    }
}
