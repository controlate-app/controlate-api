<?php

use Illuminate\Database\Seeder;
use App\Traits\SeedHelper;

class AuthorizationSeeder extends Seeder
{
    use SeedHelper;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->prepareSeed();

        foreach (config('permission.table_names') as $table) {
            DB::table($table)->truncate();
            $this->command->info("Table \"{$table}\" truncated.");
        }

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersPermissionsSeeder::class);

        $this->finishSeed();
    }
}
