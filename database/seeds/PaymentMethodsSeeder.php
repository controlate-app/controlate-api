<?php

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaultValues() as $name) {
            PaymentMethod::create(['name' => $name]);
        }
    }

    public function defaultValues()
    {
        return collect([
            'Efectivo',
            'Débito',
            'Crédito',
            'Cheque',
            'Otro',
        ]);
    }
}
