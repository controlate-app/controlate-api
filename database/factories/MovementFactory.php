<?php

use App\Models\Category;
use App\Models\Movement;
use App\Models\PaymentMethod;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Movement::class, function (Faker $faker) {
    $withCategory = $faker->boolean(95);
    return [
        'category_id' => function () use ($withCategory) {
            return $withCategory
                ? Category::inRandomOrder()->take(1)->first()->id ?? factory(Category::class)->create()->id
                : null;
        },
        'title' => function () use ($withCategory, $faker) {
            return !$withCategory ? $faker->sentence : null;
        },
        'payment_method_id' => function () use ($faker) {
            return $faker->boolean(90)
                ? PaymentMethod::inRandomOrder()->take(1)->first()->id ?? factory(PaymentMethod::class)->create()->id
                : null;
        },
        'date' => $faker->dateTimeInInterval('-6 months', '+6 months'),
        'amount' => $faker->boolean('90') ? $faker->randomFloat(2, 50, 750) : $faker->randomFloat(2, 750, 10000),
        'details' => $faker->boolean('30') ? $faker->text : null,
        'location' => function() use ($faker) {
            return $faker->boolean('75')
                ? ['address' => $faker->streetAddress, 'latitude' => $faker->latitude, 'longitude' => $faker->longitude]
                : null;
        },
        'user_id' => function () use ($faker) {
            return factory(User::class)->create()->id;
        },
    ];
});
