<?php

use App\Models\Category;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Category::class, function (Faker $faker) {
    $categories = [
        ['name' => 'Home', 'icon' => 'home'],
        ['name' => 'Car', 'icon' => 'car'],
        ['name' => 'Food', 'icon' => 'food'],
        ['name' => 'Drinks', 'icon' => 'drinks'],
        ['name' => 'Travel', 'icon' => 'plane'],
        ['name' => 'Other', 'icon' => 'question'],
    ];

    $category = $faker->randomElement($categories);

    return [
        'icon' => $category['icon'],
        'name' => $category['name'],
        'type' => $faker->boolean(85) ? 'expense' : 'income',
        'user_id' => function () use ($faker) {
            return $faker->boolean(90) ? null : factory(User::class)->create()->id;
        },
    ];
});
