<?php

use App\Models\Image;
use App\Models\Movement;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Image::class, function () {
    $img = file_get_contents('https://source.unsplash.com/random/300x300');
    $img = base64_encode($img);
    return [
        'movement_id' => function () {
            return factory(Movement::class)->create()->id;
        },
        'image' => $img,
    ];
});
