<?php

use App\Models\PaymentMethod;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(PaymentMethod::class, function (Faker $faker) {
    $paymentMethods = ['Crédito', 'Débito', 'Efectivo', 'Cheque', 'Otro'];

    return [
        'name' => $faker->randomElement($paymentMethods),
        'user_id' => function () use ($faker) {
            return $faker->boolean(90) ? null : factory(User::class)->create()->id;
        },
    ];
});
