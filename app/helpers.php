<?php

if (!function_exists('get_id_from_query')) {
    /**
     * @param \Illuminate\Http\Request|null $request
     * @return int|null
     */
    function get_id_from_query(Request $request = null)
    {
        if (empty($request)) {
            $request = request();
        }

        if (!$request->has('query') || $request->get('query') == '') {
            return null;
        }

        $query = $request->get('query');

        if (!str_contains($query, 'id: ')) {
            return null;
        }

        $afterId = str_after($query, 'id: ');
        $id = str_before($afterId, ',');

        return (int)$id;
    }
}
