<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Unique implements Rule
{
    protected $id;
    protected $user;
    protected $model;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->id = get_id_from_query();
        $this->user = auth()->user();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (new $this->model)::whereNull('deleted_at')
                ->where('name', $value)
                ->when(!empty($this->id), function ($query) {
                    return $query->where('id', '!=', $this->id);
                })
                ->count() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.unique');
    }
}
