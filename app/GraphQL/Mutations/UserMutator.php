<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Validator;

class UserMutator
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return mixed
     */
    public function update($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $this->validateUniqueEmail($args);

        if (isset($args['avatar'])) {
            $avatar = $args['avatar'];
            $args['avatar'] = $avatar->storePublicly('avatars');
        }

        $user = User::findOrFail($args['id']);
        $user->update($args);
        return $user;
    }

    /**
     * @param array $data
     */
    public function validateUniqueEmail(array $data)
    {
        $validator = Validator::make(
            $data,
            [
                'email' => ['required', Rule::unique('users', 'email')->ignore($data['id'])],
            ]
        );
        if ($validator->fails()) {
            throw ValidationException::withMessages(['email' => trans('validation.unique', ['attribute' => 'email'])]);
        }
    }
}
