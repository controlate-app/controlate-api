<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResetData extends Command
{
    const DEFAULT_DATABASE_SEEDER = 'DevSeeder';
    const ALLOWED_DATABASE_SEEDERS = [
        'DevSeeder',
        'DatabaseSeeder',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "db:reset {--seeder=". self::DEFAULT_DATABASE_SEEDER . " : The seed class}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset the application data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $seederClass = $this->option('seeder');

        if (!in_array($seederClass, self::ALLOWED_DATABASE_SEEDERS)) {
            $allowed = implode(', ', self::ALLOWED_DATABASE_SEEDERS);
            throw new \Exception("Seeder not allowed. Only following seeders are allowed: {$allowed}");
        }

        $tasks = collect([
            'migrate:fresh' => [],
            'db:seed' => [
                '--class' => $seederClass,
            ],
            'cache:clear' => [],
            'view:clear' => [],
        ]);

        if (!app()->environment('local') &&
            !$this->confirm('Are you sure of reset the application data?')
        ) {
            return;
        }

        $this->info('Executing tasks');

        $tasks->each(function ($params, $command) {
            $this->info("Execute: {$command}");
            $this->call($command, $params);
        });

        $this->line('');

        $this->info('Application data restarted!');

        return;
    }
}
