<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Movement;
use Illuminate\Auth\Access\HandlesAuthorization;

class MovementPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the movement.
     *
     * @param User $user
     * @param Movement $movement
     * @return mixed
     */
    public function view(User $user, Movement $movement)
    {
        return $user->id === $movement->user_id;
    }

    /**
     * Determine whether the user can create movements.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the movement.
     *
     * @param User $user
     * @param Movement $movement
     * @return mixed
     */
    public function update(User $user, Movement $movement)
    {
        return $user->id === $movement->user_id;
    }

    /**
     * Determine whether the user can delete the movement.
     *
     * @param User $user
     * @param Movement $movement
     * @return mixed
     */
    public function delete(User $user, Movement $movement)
    {
        return $user->id === $movement->user_id;
    }

    /**
     * Determine whether the user can restore the movement.
     *
     * @param User $user
     * @param Movement $movement
     * @return mixed
     */
    public function restore(User $user, Movement $movement)
    {
        return $user->id === $movement->user_id;
    }

    /**
     * Determine whether the user can permanently delete the movement.
     *
     * @param User $user
     * @param Movement $movement
     * @return mixed
     */
    public function forceDelete(User $user, Movement $movement)
    {
        return $user->id === $movement->user_id;
    }
}
