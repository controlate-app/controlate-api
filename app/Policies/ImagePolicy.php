<?php

namespace App\Policies;

use App\Models\Image;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ImagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the image.
     *
     * @paramUser $user
     * @paramImage $image
     * @return mixed
     */
    public function view(User $user, Image $image)
    {
        return $user->id === $image->movement->user_id;
    }

    /**
     * Determine whether the user can create images.
     *
     * @paramUser $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the image.
     *
     * @paramUser $user
     * @paramImage $image
     * @return mixed
     */
    public function update(User $user, Image $image)
    {
        return $user->id === $image->movement->user_id;
    }

    /**
     * Determine whether the user can delete the image.
     *
     * @paramUser $user
     * @paramImage $image
     * @return mixed
     */
    public function delete(User $user, Image $image)
    {
        return $user->id === $image->movement->user_id;
    }

    /**
     * Determine whether the user can restore the image.
     *
     * @paramUser $user
     * @paramImage $image
     * @return mixed
     */
    public function restore(User $user, Image $image)
    {
        return $user->id === $image->movement->user_id;
    }

    /**
     * Determine whether the user can permanently delete the image.
     *
     * @paramUser $user
     * @paramImage $image
     * @return mixed
     */
    public function forceDelete(User $user, Image $image)
    {
        return $user->id === $image->movement->user_id;
    }
}
