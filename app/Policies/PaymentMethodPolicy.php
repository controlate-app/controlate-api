<?php

namespace App\Policies;

use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentMethodPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return mixed
     */
    public function view(User $user, PaymentMethod $paymentMethod)
    {
        return $paymentMethod->user_id === null || $user->id === $paymentMethod->user_id;
    }

    /**
     * Determine whether the user can create payment methods.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return mixed
     */
    public function update(User $user, PaymentMethod $paymentMethod)
    {
        return $user->id === $paymentMethod->user_id;
    }

    /**
     * Determine whether the user can delete the payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return mixed
     */
    public function delete(User $user, PaymentMethod $paymentMethod)
    {
        return $user->id === $paymentMethod->user_id;
    }

    /**
     * Determine whether the user can restore the payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return mixed
     */
    public function restore(User $user, PaymentMethod $paymentMethod)
    {
        return $user->id === $paymentMethod->user_id;
    }

    /**
     * Determine whether the user can permanently delete the payment method.
     *
     * @param User $user
     * @param PaymentMethod $paymentMethod
     * @return mixed
     */
    public function forceDelete(User $user, PaymentMethod $paymentMethod)
    {
        return $user->id === $paymentMethod->user_id;
    }
}
