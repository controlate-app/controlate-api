<?php

namespace App\Models;

use App\Models\Traits\HasUser;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Movement extends BaseModel
{
    use HasUser;

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'double',
        'category_id' => 'integer',
        'date' => 'datetime:Y-m-d H:i:s',
        'details' => 'string',
        'location' => 'array',
        'payment_method_id' => 'integer',
        'title' => 'string',
        'user_id' => 'integer',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'category_id',
        'date',
        'details',
        'location',
        'payment_method_id',
        'title',
        'user_id',
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }

    /**
     * @return BelongsTo
     */
    public function paymentMethod(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class);
    }
}
