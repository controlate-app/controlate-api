<?php

namespace App\Models\Builders;

class Dates
{
    /**
     * @param $builder
     * @param int|null $month
     * @return mixed
     */
    public function ofMonth($builder, int $month = null)
    {
        return $builder->whereMonth('date', $month ?? date('m'));
    }

    /**
     * @param $builder
     * @param int|null $year
     * @return mixed
     */
    public function ofYear($builder, int $year = null)
    {
        return $builder->whereYear('date', $year ?? date('Y'));
    }
}
