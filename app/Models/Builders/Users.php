<?php

namespace App\Models\Builders;

class Users
{
    /**
     * @param $builder
     * @param int|null $userId
     * @return mixed
     */
    public function ofUser($builder, $userId = null)
    {
        return $builder->where('user_id', $userId ?? auth()->id());
    }

    /**
     * @param $builder
     * @param int|null $userId
     * @return mixed
     */
    public function ofUserAndGlobal($builder, $userId = null)
    {
        return $builder->where(function ($query) use ($userId) {
            $query->where('user_id', $userId ?? auth()->id())
                ->orWhereNull('user_id');
        });
    }
}
