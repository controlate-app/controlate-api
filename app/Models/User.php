<?php

namespace App\Models;

use App\Observers\UserObserver;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use HasRoles, LogsActivity, Notifiable, SoftDeletes;

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
    ];

    /**
     * @var array
     */
    protected $dates = ['last_login'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'avatar', 'password', 'status', 'last_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Boot
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(UserObserver::class);
    }

    /**
     * @return HasMany
     */
    public function movements(): HasMany
    {
        return $this->hasMany(Movement::class);
    }

    /**
     * Get all user permissions
     * @return array
     * @throws \Exception
     */
    public function getAllPermissionsAttribute(): array
    {
        return $this->getAllPermissions()->toArray();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get user full name
     * @return string
     */
    public function getNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Determines if the user is an admin
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->hasRole('ADMIN');
    }

    /**
     * Determines who can access CMS
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->isAdmin();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDefaultOrdering($query)
    {
        return $query->orderBy('last_name')
            ->orderBy('first_name');
    }

    /**
     * Users search query
     * @param $query
     * @param array $search
     * @return mixed
     */
    public function scopeMainSearch($query, array $search)
    {
        $search = collect($search);

        if ($search->has('name')) {
            $name = "%{$search->get('name')}%";
            $query->where(function ($query) use ($name) {
                $query->where('first_name', 'like', $name)
                    ->orWhere('last_name', 'like', $name);
            });
        }

        if ($search->has('email')) {
            $query->where('email', 'like', "%{$search->get('email')}%");
        }

        return $query->defaultOrdering();
    }
}
