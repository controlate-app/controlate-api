<?php

namespace App\Models;

use App\Models\Traits\HasUser;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends BaseModel
{
    use HasUser;

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'icon' => 'string',
        'name' => 'string',
        'type' => 'string',
        'user_id' => 'integer',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'icon',
        'name',
        'type',
        'user_id',
    ];

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @return HasMany
     */
    public function movements(): HasMany
    {
        return $this->hasMany(Movement::class);
    }
}
