<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Contracts\Role as RoleContract;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole implements RoleContract
{
    use LogsActivity, SoftDeletes;

    /**
     * Dates
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'title', 'guard_name'];

    /**
     * @param $query
     * @param string $search
     * @return mixed
     */
    public function scopeSearch($query, $search = '')
    {
        if ($search != '') {
            $search = "%{$search}%";
            $query->where('name', 'like', $search)
                ->orWhere('title', 'like', $search);
        }

        return $query;
    }
}
