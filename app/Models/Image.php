<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Image extends BaseModel
{
    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'movement_id' => 'integer',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'movement_id',
    ];

    /**
     * @return BelongsTo
     */
    public function movement(): BelongsTo
    {
        return $this->belongsTo(Movement::class);
    }
}
