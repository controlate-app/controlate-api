<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

trait SeedHelper
{
    public static $loggerStatus;

    public static function bootSeedHelper()
    {
        self::$loggerStatus = config('activitylog.enabled');
    }

    public function prepareSeed()
    {
        config(['activitylog.enabled' => false]);

        Model::unguard();
        Schema::disableForeignKeyConstraints();
    }

    public function finishSeed()
    {
        Schema::enableForeignKeyConstraints();
        Model::reguard();

        config(['activitylog.enabled' => self::$loggerStatus]);
    }
}
