<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Image;
use App\Models\Movement;
use App\Models\PaymentMethod;
use App\Models\User;
use App\Policies\CategoryPolicy;
use App\Policies\ImagePolicy;
use App\Policies\MovementPolicy;
use App\Policies\PaymentMethodPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Category::class => CategoryPolicy::class,
        Image::class => ImagePolicy::class,
        Movement::class => MovementPolicy::class,
        PaymentMethod::class => PaymentMethodPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
