# API Gauchitos

## Requisitos

- [Descargar Composer](https://getcomposer.org/download/) 
- [Instalar Composer globalmente](https://getcomposer.org/doc/00-intro.md#globally) 
- [Instalar Docker Compose](https://docs.docker.com/compose/install/) 

## Pasos para levantar el entorno 

Para empezar, clonamos el repo y en la carpeta raiz ejecutamos los siguientes comandos:

- composer install (solo la primera vez o cuando se actualice algún paquete)
- docker-compose build (solo la primera vez o cuando se actualice la imagen de docker)
- docker-compose start (Con esto levantamos nuestros servicios)

Con eso ya tendríamos levantado el sistema en http://localhost:8888

Cuando queremos apagar los servicios ejecutamos en la raiz del proyecto: docker-compose stop

### Ejecutar las migraciones

En ciertas ocasiones debemos correr migraciones. Para eso vamos a entrar al servicio de docker y 
ejecutar las migraciones desde adentro. Se logra con los siguientes comandos:

- docker-compose exec web bash
- php artisan db:reset

En caso de querer restablecer toda la base de datos, ejecutar:

- php artisan migrate:refresh

Para el seed de datos de pruebas de datos ejecutar:

- php artisan db:seed --class=DevSeeder
