<?php

namespace Tests\Concerns;

use App\Models\User;

trait HasUser {
    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $guard = 'api';

    /**
     * @param $override
     * @return User
     */
    public function createUser($override)
    {
        return factory(User::class)->create($override);
    }

    /**
     * @param int|null $userId
     * @param array|null $override
     * @return HasUser
     */
    public function aUserIsLoggedIn(int $userId = null, array $override = []) : self
    {
        if ($this->user) {
            return $this;
        }

        if ($userId != null) {
            $user = User::findOrFail($userId);
        } else {
            $user = $this->createUser($override);
        }

        $this->user = $user;

        $this->actingAs($this->user, $this->guard);

        return $this;
    }

}
