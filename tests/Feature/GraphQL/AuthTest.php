<?php

namespace Tests\Feature\GraphQL;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Concerns\HasUser;

class AuthTest extends GraphQLTestCase
{
    use RefreshDatabase, HasUser;

    /**
     * @return void
     */
    public function testUnauthenticatedRequestShouldFail()
    {
        $response = $this->graphQL('
            {
                me {
                    id
                    name
                }
            }
        ', false);

        $response->assertJson([
            'errors' => [
                ['debugMessage' => 'Unauthenticated.']
            ],
        ]);
    }

    /**
     * @return void
     */
    public function testAuthenticatedRequestShouldWork()
    {
        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                me {
                    id
                    name
                }
            }
        ');

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'me' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                    ]
                ]
            ]);
    }
}
