<?php

namespace Tests\Feature\GraphQL;

use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Concerns\HasUser;

class PaymentMethodsTest extends GraphQLTestCase
{
    use RefreshDatabase, HasUser;

    /**
     * @return void
     */
    public function testGetPaymentMethodById()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->create(['user_id' => $this->user->id]);

        $response = $this->graphQL('
            {
                paymentMethod(id: ' . $paymentMethod->id . ') {
                    id
                    name
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $response->assertJson([
            'data' => [
                'paymentMethod' => [
                    'id' => $paymentMethod->id,
                    'name' => $paymentMethod->name,
                    'user' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testGetOtherUserPaymentMethodByIdShouldFail()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->graphQL('
            {
                paymentMethod(id: ' . $paymentMethod->id . ') {
                    id
                    name
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ', false);

        $this->assertAuthorizationError($response, 'paymentMethod');
    }

    /**
     * @return void
     */
    public function testGetUserPaymentMethods()
    {
        $this->aUserIsLoggedIn();

        $paymentMethods = factory(PaymentMethod::class, 3)->create(['user_id' => $this->user->id]);
        $paymentMethodsNames = $paymentMethods->pluck('name');

        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                paymentMethods {
                    id
                    name
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $names = $response->json("data.paymentMethods.*.name");
        $user = $response->json("data.paymentMethods.*.user.id");

        $this->assertSame($paymentMethodsNames->all(), $names);
        $this->assertEquals($user[0], $this->user->id);
    }

    /**
     * @return void
     */
    public function testGetUserPaymentMethodsAndGlobals()
    {
        $this->aUserIsLoggedIn();

        factory(PaymentMethod::class, 3)->create(['user_id' => $this->user->id]);
        factory(PaymentMethod::class, 2)->create(['user_id' => null]);
        factory(PaymentMethod::class, 3)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                paymentMethods {
                    id
                    name
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $paymentMethods = $response->json("data.paymentMethods");

        $this->assertCount(5, $paymentMethods);
    }

    /**
     * @return void
     */
    public function testCreatePaymentMethod()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->make();

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                createPaymentMethod(
                    paymentMethod: {
                        name: "' . $paymentMethod->name . '"
                    }
                ) {
                    id
                    name
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $paymentMethodCreated = PaymentMethod::where('user_id', $this->user->id)->first();

        $this->assertEquals([
            $paymentMethod->name,
            $this->user->id,
        ], [
            $paymentMethodCreated->name,
            $paymentMethodCreated->user_id,
        ]);
    }

    /**
     * @return void
     */
    public function testUpdatePaymentMethod()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->create(['user_id' => $this->user->id]);

        $newName = 'New Name';

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                updatePaymentMethod(
                    paymentMethod: {
                        id: ' . $paymentMethod->id . '
                        name: "' . $newName . '",
                    }
                ) {
                    id
                    name
                }
            }
        ');

        $paymentMethod = $paymentMethod->fresh();

        $this->assertEquals([
            $paymentMethod->name
        ], [$newName]);
    }

    /**
     * @return void
     */
    public function testUpdateOtherUserPaymentMethodShouldFail()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->create();

        $response = $this->aUserIsLoggedIn()->graphQL('
            mutation {
                updatePaymentMethod(
                    paymentMethod: {
                        id: ' . $paymentMethod->id . '
                        name: "some name"
                    }
                ) {
                    id
                    name
                }
            }
        ', false);

        $this->assertAuthorizationError($response, 'paymentMethod');
    }

    /**
     * @return void
     */
    public function testDeletePaymentMethod()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->create(['user_id' => $this->user->id]);

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                deletePaymentMethod(id: ' . $paymentMethod->id . ') {
                    id
                    name
                }
            }
        ');

        $paymentMethod = $paymentMethod->fresh();

        $this->assertNotNull($paymentMethod->deleted_at);
    }

    /**
     * @return void
     */
    public function testDeleteOtherUserPaymentMethodShouldFail()
    {
        $this->aUserIsLoggedIn();

        $paymentMethod = factory(PaymentMethod::class)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->graphQL('
            mutation {
                deletePaymentMethod(id: ' . $paymentMethod->id . ') {
                    id
                    name
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access deletePaymentMethod');
    }
}
