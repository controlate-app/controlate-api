<?php

namespace Tests\Feature\GraphQL;

use App\Models\Category;
use App\Models\Movement;
use App\Models\PaymentMethod;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Concerns\HasUser;

class MovementsTest extends GraphQLTestCase
{
    use RefreshDatabase, HasUser;

    /**
     * @return void
     */
    public function testGetMovementById()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->create(['user_id' => $this->user->id]);

        $response = $this->graphQL('
            {
                movement(id: ' . $movement->id . ') {
                    id
                    category_id
                    payment_method_id
                    date
                    amount
                    details
                    location {
                        address
                        latitude
                        longitude
                    }
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $response->assertJson([
            'data' => [
                'movement' => [
                    'id' => $movement->id,
                    'category_id' => $movement->category_id,
                    'payment_method_id' => $movement->payment_method_id,
                    'date' => $movement->date,
                    'amount' => $movement->amount,
                    'details' => $movement->details,
                    'location' => $movement->location,
                    'user' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testGetOtherUserMovementByIdShouldFail()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->graphQL('
            {
                movement(id: ' . $movement->id . ') {
                    id
                    category_id
                    payment_method_id
                    date
                    amount
                    details
                    location {
                        address
                        latitude
                        longitude
                    }
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access movement');
    }

    /**
     * @return void
     */
    public function testGetUserMovements()
    {
        $this->aUserIsLoggedIn();

        $movements = factory(Movement::class, 10)->create(['user_id' => $this->user->id]);
        $movementsAmounts = $movements->pluck('amount');

        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                movementsPaginated {
                    data {
                        id
                        category_id
                        payment_method_id
                        date
                        amount
                        details
                        location {
                            address
                            latitude
                            longitude
                        }
                        user_id
                        user {
                            id
                            name
                        }
                    }
                }
            }
        ');

        $amounts = $response->json("data.movementsPaginated.data.*.amount");
        $user = $response->json("data.movementsPaginated.data.*.user.id");

        $this->assertSame($movementsAmounts->all(), $amounts);
        $this->assertEquals($user[0], $this->user->id);
    }

    /**
     * @return void
     */
    public function testGetUserMovementsOfTheMonth()
    {
        $this->aUserIsLoggedIn();

        factory(Movement::class, 3)->create(['user_id' => $this->user->id, 'date' => Carbon::now()->subMonth()]);
        factory(Movement::class, 5)->create(['user_id' => $this->user->id, 'date' => Carbon::now()]);
        factory(Movement::class, 3)->create(['user_id' => $this->user->id, 'date' => Carbon::now()->addMonth()]);

        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                movementsByMonth(month: '. date('n') .', year: '. date('Y') .') {
                    id
                    category_id
                    payment_method_id
                    date
                    amount
                    details
                    location {
                        address
                        latitude
                        longitude
                    }
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $movementsOfTheMonth = $response->json("data.movementsByMonth");

        $this->assertCount(5, $movementsOfTheMonth);
    }

    /**
     * @return void
     */
    public function testCreateMovement()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->make([
            'user_id' => $this->user->id,
            'category_id' => factory(Category::class)->create()->id,
            'title' => null,
            'payment_method_id' => factory(PaymentMethod::class)->create()->id,
            'location' => [
                'address' => 'Mi casa',
                'latitude' => 30.0000,
                'longitude' => -30.0000,
            ]
        ]);

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                createMovement(
                    movement: {
                        category_id: ' . $movement->category_id . '
                        payment_method_id: ' . $movement->payment_method_id . '
                        date: "' . $movement->date . '"
                        amount: ' . $movement->amount . '
                        ' . ($movement->details ? 'details: "' . $movement->details . '"' : '') . '
                        location: {
                            address: "' . $movement->location['address'] . '"
                            latitude: ' . $movement->location['latitude'] . '
                            longitude: ' . $movement->location['longitude'] . '
                        }
                    }
                ) {
                    id
                    category_id 
                    category {
                        id
                        name
                    }
                    payment_method_id 
                    payment_method {
                        id
                        name
                    }
                    date
                    amount
                    details
                    location {
                        address
                        latitude
                        longitude
                    }
                    user_id
                    user {
                        id
                        name
                    }
                    created_at
                    updated_at
                    deleted_at
                }
            }
        ');

        $movementCreated = Movement::where('user_id', $this->user->id)->first();

        $this->assertEquals(
            $movement->toArray(),
            array_except(
                $movementCreated->toArray(),
                ['id', 'created_at', 'updated_at', 'deleted_at']
            )
        );
    }

    /**
     * @return void
     */
    public function testCreateMovementWithoutCategoryRequireTitle()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->make([
            'user_id' => $this->user->id,
            'category_id' => null,
            'title' => null,
            'payment_method_id' => factory(PaymentMethod::class)->create()->id,
            'location' => [
                'address' => 'Mi casa',
                'latitude' => 30.0000,
                'longitude' => -30.0000,
            ]
        ]);

        $response = $this->aUserIsLoggedIn()->graphQL('
            mutation {
                createMovement(
                    movement: {
                        payment_method_id: ' . $movement->payment_method_id . '
                        date: "' . $movement->date . '"
                        amount: ' . $movement->amount . '
                    }
                ) {
                    id
                }
            }
        ', false);

        $this->assertErrorMessage($response, "Validation failed for the field [createMovement].");
        $this->assertValidationErrors($response, [
            'movement.category_id' => [
                'El campo movement.category id es obligatorio cuando movement.title no está presente.',
            ],
            'movement.title' => [
                'El campo movement.title es obligatorio cuando movement.category id no está presente.',
            ],
        ]);

        $movement = factory(Movement::class)->make([
            'user_id' => $this->user->id,
            'category_id' => null,
            'title' => "Titulo de prueba",
            'payment_method_id' => factory(PaymentMethod::class)->create()->id,
            'location' => [
                'address' => 'Mi casa',
                'latitude' => 30.0000,
                'longitude' => -30.0000,
            ]
        ]);

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                createMovement(
                    movement: {
                        title: "' . $movement->title . '"
                        payment_method_id: ' . $movement->payment_method_id . '
                        date: "' . $movement->date . '"
                        amount: ' . $movement->amount . '
                    }
                ) {
                    id
                    category_id 
                    category {
                        id
                        name
                    }
                    amount
                }
            }
        ');

        $movementCreated = Movement::where('user_id', $this->user->id)->first();

        $this->assertEquals(
            [
                'title' => $movement->title,
                'amount' => $movement->amount,
                'category_id' => $movement->category_id,
            ],
            [
                'title' => $movementCreated->title,
                'amount' => $movementCreated->amount,
                'category_id' => $movementCreated->category_id,
            ]
        );
    }

    /**
     * @return void
     */
    public function testUpdateMovement()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->create(['user_id' => $this->user->id]);

        $newCategory = factory(Category::class)->create(['user_id' => null])->id;
        $newAmount = 1500.50;
        $newDetails = 'New Icon';

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                updateMovement(
                    movement: {
                        id: ' . $movement->id . '
                        category_id: ' . $newCategory . ',
                        amount: ' . $newAmount . ',
                        details: "' . $newDetails . '",
                    }
                ) {
                    id
                    amount
                }
            }
        ');

        $movement = $movement->fresh();

        $this->assertEquals([
            $movement->category_id,
            $movement->amount,
            $movement->details,
        ], [
            $newCategory,
            $newAmount,
            $newDetails,
        ]);
    }

    /**
     * @return void
     */
    public function testUpdateOtherUserMovementShouldFail()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->create();

        $response = $this->aUserIsLoggedIn()->graphQL('
            mutation {
                updateMovement(
                    movement: {
                        id: ' . $movement->id . '
                        amount: 300.5
                    }
                ) {
                    id
                    amount
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access updateMovement');
    }

    /**
     * @return void
     */
    public function testDeleteMovement()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->create(['user_id' => $this->user->id]);

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                deleteMovement(id: ' . $movement->id . ') {
                    id
                    date
                    amount
                }
            }
        ');

        $movement = $movement->fresh();

        $this->assertNotNull($movement->deleted_at);
    }

    /**
     * @return void
     */
    public function testDeleteOtherUserMovementShouldFail()
    {
        $this->aUserIsLoggedIn();

        $movement = factory(Movement::class)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->graphQL('
            mutation {
                deleteMovement(id: ' . $movement->id . ') {
                    id
                    amount
                    date
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access deleteMovement');
    }
}
