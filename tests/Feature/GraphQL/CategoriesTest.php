<?php

namespace Tests\Feature\GraphQL;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Concerns\HasUser;

class CategoriesTest extends GraphQLTestCase
{
    use RefreshDatabase, HasUser;

    /**
     * @return void
     */
    public function testGetCategoryById()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->create(['user_id' => $this->user->id]);

        $response = $this->graphQL('
            {
                category(id: ' . $category->id . ') {
                    id
                    name
                    icon
                    type
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $response->assertJson([
            'data' => [
                'category' => [
                    'id' => $category->id,
                    'name' => $category->name,
                    'icon' => $category->icon,
                    'type' => $category->type,
                    'user' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testGetOtherUserCategoryByIdShouldFail()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->graphQL('
            {
                category(id: ' . $category->id . ') {
                    id
                    name
                    icon
                    type
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access category');
    }

    /**
     * @return void
     */
    public function testGetUserCategories()
    {
        $this->aUserIsLoggedIn();

        $categories = factory(Category::class, 3)->create(['user_id' => $this->user->id]);
        $categoriesNames = $categories->pluck('name');

        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                categories {
                    id
                    name
                    icon
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $names = $response->json("data.categories.*.name");
        $user = $response->json("data.categories.*.user.id");

        $this->assertSame($categoriesNames->all(), $names);
        $this->assertEquals($user[0], $this->user->id);
    }

    /**
     * @return void
     */
    public function testGetUserCategoriesAndGlobals()
    {
        $this->aUserIsLoggedIn();

        factory(Category::class, 3)->create(['user_id' => $this->user->id]);
        factory(Category::class, 2)->create(['user_id' => null]);
        factory(Category::class, 3)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                categories {
                    id
                    name
                    icon
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $categories = $response->json("data.categories");

        $this->assertCount(5, $categories);
    }

    /**
     * @return void
     */
    public function testCreateCategory()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->make();

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                createCategory(
                    category: {
                        name: "' . $category->name . '",
                        icon: "' . $category->icon . '",
                    }
                ) {
                    id
                    name
                    icon
                    user {
                        id
                        name
                    }
                }
            }
        ');

        $categoryCreated = Category::where('user_id', $this->user->id)->first();

        $this->assertEquals([
            $category->name,
            $category->icon,
            $this->user->id,
        ], [
            $categoryCreated->name,
            $categoryCreated->icon,
            $categoryCreated->user_id,
        ]);
    }

    /**
     * @return void
     */
    public function testUpdateCategory()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->create(['user_id' => $this->user->id]);

        $newName = 'New Name';
        $newIcon = 'New Icon';

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                updateCategory(
                    category: {
                        id: ' . $category->id . '
                        name: "' . $newName . '",
                        icon: "' . $newIcon . '",
                    }
                ) {
                    id
                    name
                    icon
                }
            }
        ');

        $category = $category->fresh();

        $this->assertEquals([
            $category->name,
            $category->icon,
        ], [
            $newName,
            $newIcon,
        ]);
    }

    /**
     * @return void
     */
    public function testUpdateOtherUserCategoryShouldFail()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->create();

        $response = $this->aUserIsLoggedIn()->graphQL('
            mutation {
                updateCategory(
                    category: {
                        id: ' . $category->id . '
                        name: "some name"
                    }
                ) {
                    id
                    name
                    icon
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access updateCategory');
    }

    /**
     * @return void
     */
    public function testDeleteCategory()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->create(['user_id' => $this->user->id]);

        $this->aUserIsLoggedIn()->graphQL('
            mutation {
                deleteCategory(id: ' . $category->id . ') {
                    id
                    name
                    icon
                }
            }
        ');

        $category = $category->fresh();

        $this->assertNotNull($category->deleted_at);
    }

    /**
     * @return void
     */
    public function testDeleteOtherUserCategoryShouldFail()
    {
        $this->aUserIsLoggedIn();

        $category = factory(Category::class)->create(['user_id' => factory(User::class)->create()->id]);

        $response = $this->graphQL('
            mutation {
                deleteCategory(id: ' . $category->id . ') {
                    id
                    name
                    icon
                    type
                    user_id
                    user {
                        id
                        name
                    }
                }
            }
        ', false);

        $this->assertAuthorizationError($response);
        $this->assertAuthorizationErrorMessage($response, 'You are not authorized to access deleteCategory');
    }
}
