<?php

namespace Tests\Feature\GraphQL;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Concerns\HasUser;

class UsersTest extends GraphQLTestCase
{
    use RefreshDatabase, HasUser;

    /**
     * @return void
     */
    public function testGetUserById()
    {
        $response = $this->aUserIsLoggedIn()->graphQL('
            {
                user (id: ' . $this->user->id . ') {
                    id
                    name
                }
            }
        ');

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'user' => [
                        'id' => $this->user->id,
                        'name' => $this->user->name,
                    ]
                ]
            ]);
    }

    /**
     * @return void
     */
    public function testGetUsers()
    {
        $users = factory(User::class, 10)->create();
        $createdNames = $users->sortBy('last_name')->pluck('name');

        $response = $this->actingAs($users->first(), 'api')->graphQL('
            {
                users {
                    data {
                        id
                        name
                    },
                    paginatorInfo{
                        count
                        currentPage
                        total
                        lastPage
                    }
                }
            }
        ');

        $response->assertStatus(200);

        $names = $response->json("data.users.data.*.name");

        $this->assertSame($createdNames->all(), $names);
    }

    /**
     * @return void
     */
    public function testGetUsersByName()
    {
        factory(User::class)->create(['first_name' => 'Nombre', 'last_name' => 'Apellido']);
        factory(User::class)->create(['first_name' => 'Otro', 'last_name' => 'Otro']);
        $user = factory(User::class)->create(['first_name' => 'Lionel', 'last_name' => 'Messi']);

        $response = $this->actingAs($user, 'api')->graphQL('
            {
                users (name: "Messi") {
                    data {
                        id
                        name
                    },
                    paginatorInfo{
                        count
                        currentPage
                        total
                        lastPage
                    }
                }
            }
        ');

        $response->assertStatus(200)->assertJson([
            'data' => [
                'users' => [
                    'data' => [
                        [
                            'name' => 'Lionel Messi'
                        ]
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testGetUsersByEmail()
    {
        factory(User::class)->create(['email' => 'unemail@gmail.com']);
        factory(User::class)->create(['email' => 'otroemail@hotmail.com']);
        $user = factory(User::class)->create(
            ['first_name' => 'Lionel', 'last_name' => 'Messi', 'email' => 'leomessi@gmail.com']
        );

        $response = $this->actingAs($user, 'api')->graphQL('
            {
                users (email: "leomessi@gmail.com") {
                    data {
                        id
                        email
                        name
                    },
                    paginatorInfo{
                        count
                        currentPage
                        total
                        lastPage
                    }
                }
            }
        ');

        $response->assertStatus(200)->assertJson([
            'data' => [
                'users' => [
                    'data' => [
                        [
                            'name' => 'Lionel Messi',
                            'email' => 'leomessi@gmail.com'
                        ]
                    ]
                ]
            ]
        ]);
    }
}
