<?php

namespace Tests\Feature\GraphQL;

use Illuminate\Foundation\Testing\TestResponse;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Tests\TestCase;

abstract class GraphQLTestCase extends TestCase
{
    use MakesGraphQLRequests;

    /**
     * Execute a query as if it was sent as a request to the server.
     *
     * @param string $query
     * @param bool $checkNoErrors
     * @return TestResponse
     */
    protected function graphQL(string $query, $checkNoErrors = true): TestResponse
    {
        $response = $this->postGraphQL(
            [
                'query' => $query,
            ]
        );

        $response->assertStatus(200);

        if ($checkNoErrors) {
            $this->assertNoGraphQLErrors($response);
        }

        return $response;
    }

    /**
     * @param TestResponse $response
     * @return void
     */
    public function assertNoGraphQLErrors(TestResponse $response): void
    {
        $this->assertNull(
            $response->json('errors'),
            "GraphQL response has errors"
        );
        return;
    }

    /**
     * @param TestResponse $response
     * @return void
     */
    public function assertAuthorizationError(TestResponse $response): void
    {
        $this->assertEquals($response->json("errors.*.extensions.category"), ['authorization']);
        return;
    }

    /**
     * @param TestResponse $response
     * @param string $message
     * @return void
     */
    public function assertErrorMessage(TestResponse $response, $message): void
    {
        $this->assertEquals($response->json("errors.*.message"), [$message]);
        return;
    }

    /**
     * @param TestResponse $response
     * @param string $message
     */
    public function assertAuthorizationErrorMessage(TestResponse $response, $message): void
    {
        $this->assertErrorMessage($response, $message);
        return;
    }

    /**
     * @param TestResponse $response
     * @param array $errors
     * @return $this
     */
    public function assertValidationErrors(TestResponse $response, array $errors)
    {
        $this->assertEquals($response->json('errors.*.extensions.validation')[0], $errors);
        return $this;
    }
}
